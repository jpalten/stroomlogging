<?php
/**
 * Created by PhpStorm.
 * User: jelle
 * Date: 02-06-15
 * Time: 22:52
 */

define('MINUTE',60);
define('EXPECTED_MAX_INTERVAL',6);
//define('TIME_INTERVAL','+5 minutes');
define('TIME_INTERVAL','+10 minutes');
//define('TIME_INTERVAL','+20 minutes');
//define('TIME_INTERVAL','+30 minutes');
//define('TIME_INTERVAL','+60 minutes');

define('TAB_SEPARATED_HEADER_PEL103','Datum	Tijd	V1	V1 1s MIN	V1 Tijd van 1s MIN	V1 1s MAX	V1 Tijd van 1s MAX	V2	V2 1s MIN	V2 Tijd van 1s MIN	V2 1s MAX	V2 Tijd van 1s MAX	V3	V3 1s MIN	V3 Tijd van 1s MIN	V3 1s MAX	V3 Tijd van 1s MAX	U12	U12 1s MIN	U12 Tijd van 1s MIN	U12 1s MAX	U12 Tijd van 1s MAX	U23	U23 1s MIN	U23 Tijd van 1s MIN	U23 1s MAX	U23 Tijd van 1s MAX	U31	U31 1s MIN	U31 Tijd van 1s MIN	U31 1s MAX	U31 Tijd van 1s MAX	I1	I1 1s MIN	I1 Tijd van 1s MIN	I1 1s MAX	I1 Tijd van 1s MAX	I2	I2 1s MIN	I2 Tijd van 1s MIN	I2 1s MAX	I2 Tijd van 1s MAX	I3	I3 1s MIN	I3 Tijd van 1s MIN	I3 1s MAX	I3 Tijd van 1s MAX	IN	IN 1s MIN	IN Tijd van 1s MIN	IN 1s MAX	IN Tijd van 1s MAX	V1-CF	V2-CF	V3-CF	I1-CF	I2-CF	I3-CF	F	F 1s MIF Tijd van 1s MIN	F 1s MAX	F Tijd van 1s MAX	P1	P2	P3	PT	P1-	P2-	P3-	PT-	P1+	P2+	P3+	PT+	Q1	Q2	Q3	QT	Q1-	Q2-	Q3-	QT-	Q1+	Q2+	Q3+	QT+	S1	S2	S3	ST	PF1-	PF2-	PF3-	PFT-	PF1+	PF2+	PF3+	PFT+	Cos _1-Cos _2-	Cos _3-	Cos _T-	Q Cos _1-	Q Cos _2-	Q Cos _3-	Q Cos _T-	Cos _1+	Cos _2+	Cos _3+	Cos _T+Q Cos _1+	Q Cos _2+	Q Cos _3+	Q Cos _T+	Tan _-	Tan _+	V1-THD	V2-THD	V3-THD	U12-THD	U23-THDU31-THD	I1-THD	I2-THD	I3-THD	IN-THD	Ep	Ep+	Ep-	Eq	Eq/q1	Eq/q2	Eq/q3	Eq/q4	Es	Es+	Es-');
define('TAB_SEPARATED_HEADER_KWX63','SN;Date;Time;V1N;V2N;V3N;V12;V23;V31;VSYS;A1;A2;A3;AN;ASYS;PF1;PF2;PF3;PFSYS;P1;P2;P3;PSYS;S1;S2;S3;SSYS;Q1;Q2;Q3;QSYS;F;PHASE_SEQ;kWhSYS_imp;kWh SYS_exp');

define('KWX63','KWX63');
define('PEL103','PEL103');
define('EMS96','EMS96');

define('HEADER',"Date\tA1\tA2\tA3\n");

date_default_timezone_set("UTC");

class Parser {

    /** @field $startTime DateTime */
    public $startTime;
    public $nextTime;
    public $maxvalues;
    public $prevTime;
    private $split_regex = "/;/";
    private $parse_mode = KWX63;
    public $column_defs = array(KWX63 => array(1,2,10,11,12),
        PEL103 => array(0,1,35,40,45));


    function parseFile($filename) {

        $this->resetMaxValues();

        $content = file_get_contents($filename);
        $lines = preg_split('/\R/',$content);

//        echo HEADER;

        $firstline = array_shift($lines);
//        echo $firstline , "\n";
//        exit();


        if (strpos($firstline,"\t")!==FALSE) {
            $this->setParseType(PEL103);
            array_shift($lines);
//            echo "tabs!\n";exit();
        } elseif (strpos($firstline,"Smart Log")!==FALSE) {
            $this->setParseType(EMS96);
            $lines = array_slice($lines,9);
            $firstline = array_shift($lines);
            $this->findColumns($firstline);
            $lines = array_slice($lines,1);
//            echo $lines[0]; exit();

        } else {
//            echo "'$firstline'"; exit;
        }

        $results = array();
        {
            $parts = preg_split($this->split_regex,$firstline);
            list($date_column,$time_column,$a1_column,$a2_column,$a3_column) = $this->column_defs[$this->parse_mode];

            $columns = array($parts[$date_column] . "/" . $parts[$time_column], $parts[$a1_column], $parts[$a2_column], $parts[$a3_column]);
            $results[] = join("\t",$columns);
        }

//        echo "parse ", count($lines), " lines? \n";
        foreach ($lines as $line) {

            if ($this->prevTime==NULL) {
                $this->initStartTimeFromFirstLine($line);
            }

            $lines = $this->parseLine($line);
            $results = array_merge($results,$lines);
        }
        if ($this->nextTime!=NULL) {// && $this->nextTime!=$this->prevTime) {
            $results[] = $this->reportMaxValue($this->nextTime,$this->maxvalues);
        }
        return $results;
    }


    private function parseLine($linestr)
    {
        global $timeInterval;
        $resultslines = array();

        $line = new Line($linestr,$this->split_regex,$this->column_defs[$this->parse_mode]);

        if ($line->isOk) {
            if ($this->startTime == NULL) {
                $this->startTime = $line->dateTime;

                $this->nextTime = $this->startTime;
                $this->nextTime->modify(TIME_INTERVAL);
            }

            /** @var $difference DateInterval */
            $difference = $this->startTime->diff($line->dateTime);
//            $differenceInSeconds = $this->differenceInSeconds( $difference);
//            echo "diff: " , $differenceInSeconds, "\n";

            debug ("parseLine $linestr" );
            if ($line->dateTime>$this->nextTime) {
                debug ("next time reached");
                $resultslines[] = $this->reportMaxValue($this->nextTime,$this->maxvalues);

                $this->resetMaxValues();
                $this->startTime = $this->nextTime;

                $this->nextTime->modify(TIME_INTERVAL);

                while ($line->dateTime>$this->nextTime) {
                    debug("more empty lines");
                    $resultslines[] = $this->reportMaxValue($this->nextTime,$this->maxvalues);
                    $this->startTime = $this->nextTime;
                    $this->nextTime->modify(TIME_INTERVAL);
                }
            }

//            if ($this->prevTime!=NULL) {
//                $difference = $this->prevTime->diff($line->dateTime);
//                $differenceInSeconds = $this->differenceInSeconds( $difference);
//                if ($differenceInSeconds > EXPECTED_MAX_INTERVAL) {
//                    $this->reportMaxValue($this->prevTime,new Line() );
//                    $this->reportMaxValue($line->dateTime,new Line() );
//                }
//            }

            $this->prevTime = $line->dateTime;
            $this->checkMaxValues($line);
        }

        return $resultslines;
    }

    private function resetMaxValues()
    {
        $this->maxvalues = new Line();
    }

    private function checkMaxValues($line)
    {
        if ($this->maxvalues==NULL) {
            echo "nothing yet\n";
            return;
        }

        if ($line->A1 > $this->maxvalues->A1) {
            $this->maxvalues->A1 = $line->A1;
        }
        if ($line->A2 > $this->maxvalues->A2) {
            $this->maxvalues->A2 = $line->A2;
        }
        if ($line->A3 > $this->maxvalues->A3) {
            $this->maxvalues->A3 = $line->A3;
        }
    }

    private function reportMaxValue($dateTime, $maxValues)
    {
        if ($dateTime==NULL) {
            var_dump($this);
            debug_print_backtrace();
            echo "oei";poop();exit();
        }

        /** @var $dateTime DateTime */

        $values = array($maxValues->A1,$maxValues->A2,$maxValues->A3);
        debug("report " . join("\t",$values));
        $valuesStr = str_replace('.',',', join("\t", $values));
        return $dateTime->format('d-m-Y H:i:s') . "\t" .  $valuesStr;
    }

    private function differenceInSeconds($difference)
    {
        return ($difference->y * 365 * 24 * 60 * 60) +
            ($difference->m * 30 * 24 * 60 * 60) +
            ($difference->d * 24 * 60 * 60) +
            ($difference->h * 60 * 60) +
            ($difference->i * 60) +
            $difference->s;
    }

    private function setParseType($type) {
        $this->parse_mode = $type;
        switch ($type) {
            case PEL103:
                $this->split_regex = "/\t/";
                break;
            case EMS96:
                $this->split_regex = "/\s\s+/";
                break;
        }
    }

    private function findColumns($header)
    {
        $column_defs = array();
        $parts = preg_split($this->split_regex,$header);
        foreach ($parts as $index=>$headingName) {
            switch ($headingName) {
                case "Date": $column_defs[0] = $index; break;
                case "Time": $column_defs[1] = $index; break;
                case "A L1 [A] - Max": $column_defs[2] = $index; break;
                case "A L2 [A] - Max": $column_defs[3] = $index; break;
                case "A L3 [A] - Max": $column_defs[4] = $index; break;
            }
        }
        $this->column_defs[EMS96] = $column_defs;
    }

    /**
     * @param $line string
     */
    public function initStartTimeFromFirstLine($line)
    {
//                echo "split '$this->split_regex'"; exit();

        $parts = preg_split($this->split_regex, $line);
//                print_r($parts);
//                exit();

        if (count($parts) > 2) {

            list($date_column, $time_column, $a1_column, $a2_column, $a3_column) = $this->column_defs[$this->parse_mode];

            $datestr = $parts[$date_column] . " " . $parts[$time_column];
            $pos = strpos($datestr, ':');
            $datestr = substr($datestr, 0, $pos) . ':00:00';

            $this->startTime = DateTime::createFromFormat('j-m-y H:i:s', $datestr);
            if ($this->startTime == FALSE) {
                $this->startTime = DateTime::createFromFormat('j-m-Y H:i:s', $datestr);
            }
            if ($this->startTime == FALSE) {
                $this->startTime = DateTime::createFromFormat('j/m/Y H:i:s', $datestr);
            }
            $this->nextTime = $this->startTime;
//            $this->nextTime->modify(TIME_INTERVAL);
        }
    }
}


class Line {
    public $A1 = 0.0;
    public $A2 = 0.0;
    public $A3 = 0.0;

    public $isOk = FALSE;
    public $dateTime = NULL;
    public $split_regex = "/;/";


    function __construct($line = NULL, $field_regex=NULL, $columns = array(1,2,10,11,12)) {
        if ($field_regex!=NULL) {
            $this->split_regex = $field_regex;
        }

        list($date_column,$time_column,$a1_column,$a2_column,$a3_column) = $columns;

        $parts = preg_split($this->split_regex,$line);
        if (count($parts)>12 && $parts[0]!='') {
            $this->parseDateTime($parts[$date_column],$parts[$time_column] );
            $this->A1 = 0.0 + str_replace(',','.',$parts[$a1_column]);
            $this->A2 = 0.0 + str_replace(',','.',$parts[$a2_column]);
            $this->A3 = 0.0 + str_replace(',','.',$parts[$a3_column]);

            $this->isOk = TRUE;
        }
    }

    private function parseDateTime($dateStr, $timeStr)
    {
        $this->dateTime = DateTime::createFromFormat('j-m-y H:i:s', "$dateStr $timeStr");
        if ($this->dateTime==FALSE) {
            $this->dateTime = DateTime::createFromFormat('j-m-Y H:i:s', "$dateStr $timeStr");
        }
        if ($this->dateTime==FALSE) {
            $this->dateTime = DateTime::createFromFormat('j/m/Y H:i:s', "$dateStr $timeStr");
        }
    }

}


function debug($text) {
    echo "$text\n";
}