<?php

require_once dirname(__FILE__) . "/tests_setup.inc";

class ParserTests extends PHPUnit_Framework_TestCase {

    public function testPel103() {
        $parser = new Parser();
        $lines = $parser->parseFile("fixtures/PEL103.txt");
        $this->assertEquals(12,count($lines));
    }

    public function testKwx63() {
        $parser = new Parser();
        $lines = $parser->parseFile("fixtures/KWX63.csv");
        $this->assertEquals(275,count($lines));
    }

    public function testEms96() {
        $parser = new Parser();
        $lines = $parser->parseFile("fixtures/EMS96_00.txt");
        $expected_results = array("Date/Time\tA L1 [A] - Max\tA L2 [A] - Max\tA L3 [A] - Max",
            join("\t",array("13-05-2015 20:00:00","37,55","1","9")),
            join("\t",array("13-05-2015 20:10:00","26,091","3","8")),
            join("\t",array("13-05-2015 20:20:00","37,565","5","6")),
            join("\t",array("13-05-2015 20:30:00","29,324","7","4")),
            join("\t",array("13-05-2015 20:40:00","29,523","9","2")),
        );
        $this->assertEquals($expected_results,$lines);
    }

    public function testEms96_05() {
        $parser = new Parser();
        $lines = $parser->parseFile("fixtures/EMS96_05.txt");
        $expected_results = array("Date/Time\tA L1 [A] - Max\tA L2 [A] - Max\tA L3 [A] - Max",
            join("\t",array("13-05-2015 20:00:00","0","0","0")),
            join("\t",array("13-05-2015 20:10:00","26,091","3","8")),
            join("\t",array("13-05-2015 20:20:00","37,565","5","6")),
            join("\t",array("13-05-2015 20:30:00","29,324","7","4")),
            join("\t",array("13-05-2015 20:40:00","29,523","9","2")),
            join("\t",array("13-05-2015 20:50:00","37,55","10","0")),
        );
        $this->assertEquals($expected_results,$lines);
    }

    public function testEms96_25() {
        $parser = new Parser();
        $lines = $parser->parseFile("fixtures/EMS96_25.txt");
        $expected_results = array("Date/Time\tA L1 [A] - Max\tA L2 [A] - Max\tA L3 [A] - Max",
            join("\t",array("13-05-2015 20:00:00","0","0","0")),
            join("\t",array("13-05-2015 20:10:00","0","0","0")),
            join("\t",array("13-05-2015 20:20:00","0","0","0")),
            join("\t",array("13-05-2015 20:30:00","29,324","6","4")),
        );
        $this->assertEquals($expected_results,$lines);
    }

    public function testEms96_findColumns() {
        $parser = new Parser();
        $lines = $parser->parseFile("fixtures/EMS96_00.txt");
        $expected_column_defs = array(1,2,3,6,9);
        $expected_first_line = "Date/Time\tA L1 [A] - Max\tA L2 [A] - Max\tA L3 [A] - Max";

        $this->assertEquals($expected_first_line,$lines[0]);
        $this->assertEquals($expected_column_defs,$parser->column_defs[EMS96]);
    }

}

